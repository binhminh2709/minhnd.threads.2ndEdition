package test;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.junit.Assert;
import org.junit.Test;

import util.IOUtil;

public class UnitTest {
  
  @Test
  public void testIO() throws IOException {
    InputStream inputStream = IOUtil.loadRes("classpath:image/Chrysanthemum.jpg");
    Assert.assertNotNull(inputStream);
    
    // System.out.println(IOUtil.loadResAsString("classpath:image/Chrysanthemum.jpg"));
    BufferedImage imgBuff = ImageIO.read(inputStream);
    System.out.println("Height:" + imgBuff.getHeight() + "\nWeight:" + imgBuff.getWidth());
    Image img = ImageIO.read(inputStream);
    Assert.assertNotNull(img);
    
  }
}
